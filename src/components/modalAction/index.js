import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";

const useStyles = makeStyles((theme) => ({
  box: {
    backgroundColor: "#FFFFFF",
    padding: theme.spacing(2),
  },
  button: {
    margin: theme.spacing(1),
  },
}));

const ModalAction = ({ title, subtitle, isOpen, onConfirm, onClose }) => {
  const classes = useStyles();
  return (
    <Modal open={isOpen} onClose={onClose}>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{ minHeight: "100vh", padding: "0px 24px" }}
      >
        <Box
          borderRadius="4px"
          display="flex"
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
          className={classes.box}
        >
          <Typography component="h2" color="secondary">
            {title}
          </Typography>
          <Typography>{subtitle}</Typography>
          <Box>
            <Button
              className={classes.button}
              type="submit"
              variant="contained"
              color="secondary"
              onClick={onClose}
            >
              Voltar
            </Button>
            <Button
              className={classes.button}
              type="submit"
              variant="contained"
              color="primary"
              onClick={onConfirm}
            >
              Confirmar
            </Button>
          </Box>
        </Box>
      </Grid>
    </Modal>
  );
};
export { ModalAction };
