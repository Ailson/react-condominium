import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import React from "react";

const PageContainer = ({ title, children }) => {
  return (
    <Box
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      paddingBottom="32px"
    >
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        height="10vh"
        marginTop="64px"
      >
        <Typography component="h1" variant="h5">
          {title}
        </Typography>
      </Box>
      <Box
        display="flex"
        flexDirection="row"
        flexWrap="wrap"
        minHeight="55vh"
        justifyContent="center"
        alignItems="center"
      >
        {children}
      </Box>
    </Box>
  );
};

export { PageContainer };
