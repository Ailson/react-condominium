import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import { deleteApartment, getApartments } from "../../services/apartments";
import { List } from "../list";
import { ModalAction } from "../modalAction";
import { PageContainer } from "../pageContainer";

const ApartmentsList = withRouter(({ history }) => {
  const [apartmentsBlockA, setApartmentsBlockA] = useState([]);
  const [apartmentsBlockB, setApartmentsBlockB] = useState([]);
  const [apartmentId, setApartmentId] = useState("");
  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    async function fetchData() {
      await handleApartments();
    }
    fetchData();
  }, []);

  const createItem = (apartment) => {
    return {
      id: apartment.id,
      data: `N°: ${apartment.number}, Responável: ${
        apartment.has_responsible ? "Sim" : "Não"
      }`,
    };
  };

  const handleApartments = async () => {
    const response = await getApartments();
    if (response?.data) {
      setApartmentsBlockA(
        response.data
          .sort((a, b) => a.number - b.number)
          .filter((apartments) => apartments.block === "A")
          .map(createItem)
      );
      setApartmentsBlockB(
        response.data
          .sort((a, b) => a.number - b.number)
          .filter((apartments) => apartments.block === "B")
          .map(createItem)
      );
    }
  };

  const openConfirmApartmentDelete = (apartmentId) => {
    setApartmentId(apartmentId);
    setOpenModal(true);
  };

  const goToCreateApartment = () => history.push("/home/create_apartment");

  const goToEditApartment = (apartmentId) =>
    history.push(`/home/edit_apartment/${apartmentId}`);

  const goToResidentsList = (apartmentId) =>
    history.push(`/home/residents/${apartmentId}`);

  const closeModal = () => setOpenModal(false);

  const handleDeleteApartment = async () => {
    await deleteApartment(apartmentId);
    await handleApartments();
    setOpenModal(false);
  };

  return (
    <PageContainer title="Lista de Apartamentos">
      <List
        title="Bloco A"
        createText="Apartamento"
        onCreate={goToCreateApartment}
        items={apartmentsBlockA}
        onDelete={(apartmentId) => openConfirmApartmentDelete(apartmentId)}
        onEdit={(apartmentId) => goToEditApartment(apartmentId)}
        onGoTo={(apartmentId) => goToResidentsList(apartmentId)}
      />
      <List
        title="Bloco B"
        createText="Apartamento"
        onCreate={goToCreateApartment}
        items={apartmentsBlockB}
        onDelete={(apartmentId) => openConfirmApartmentDelete(apartmentId)}
        onEdit={(apartmentId) => goToEditApartment(apartmentId)}
        onGoTo={(apartmentId) => goToResidentsList(apartmentId)}
      />
      <ModalAction
        isOpen={openModal}
        title={"Atenção!"}
        subtitle="Esta ação apagará todos os moradores deste apartamento também."
        onConfirm={handleDeleteApartment}
        onClose={closeModal}
      />
    </PageContainer>
  );
});

export { ApartmentsList };
