import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import Radio from "@material-ui/core/Radio";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import React, { useEffect, useState } from "react";
import { useParams, withRouter } from "react-router-dom";
import { getResidentById, updateResident } from "../../services/residents";
import { maskBirth, maskPhoneNumber } from "../../utils/helpers";
import { PageContainer } from "../pageContainer";

const EditResident = withRouter(({ history }) => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [document, setDocument] = useState("");
  const [phone, setPhone] = useState("");
  const [birth, setBirth] = useState("");
  const [isResponsible, setIsResponsible] = useState(true);
  const [apartment, setApartment] = useState();
  const [apartments, setApartments] = useState([]);
  const [errorMensage, setErrorMessage] = useState("");
  const matches = useMediaQuery("(min-width:600px)");
  const { id } = useParams();

  useEffect(() => {
    async function fetchData() {
      await getResident();
    }
    fetchData();
  }, []);

  const getResident = async () => {
    const response = await getResidentById(id);

    if (typeof response === "string") return;

    const apartments =
      response.data.apartments.length > 0 &&
      response.data.apartments
        .sort((a, b) =>
          a.id === response.data.resident.apartment_id
            ? -1
            : b.id === response.data.resident.apartment_id
            ? 1
            : 0
        )
        .map((apartment) => {
          return {
            id: apartment.id,
            selectItem: `Bloco: ${apartment.block}, N° ${apartment.number}`,
          };
        });

    setUsername(response.data.resident.username);
    setEmail(response.data.resident.email);
    setDocument(response.data.resident.document);
    setPhone(response.data.resident.phone);
    setBirth(
      response.data.resident.birth.split("T")[0].split("-").reverse().join("/")
    );
    setIsResponsible(response.data.resident.is_responsible);
    setApartment(apartments[0]);
    setApartments(apartments);
  };

  const handleUsername = (e) => setUsername(e.target.value);

  const handlePhone = (e) => setPhone(maskPhoneNumber(e.target.value));

  const handleBirth = (e) => setBirth(maskBirth(e.target.value));

  const handleIsResponsible = () => setIsResponsible(!isResponsible);

  const handleApartmentSelect = (e) => setApartment(e.target.value);

  const handleEditResident = async () => {
    if (!birth || !phone || !document) {
      setErrorMessage("Preencha todos os campos corretamente");
      return;
    }

    const response = await updateResident({
      username,
      birth: birth.split("/").reverse().join("-"),
      phone,
      is_responsible: isResponsible,
      apartment_id: apartment.id,
      id,
    });

    if (typeof response.data === "string") {
      setErrorMessage(response.data);
    } else {
      history.goBack();
    }
  };

  return (
    <PageContainer title="Editar Morador">
      <Paper elevation={3}>
        <Box
          padding="24px"
          width={matches ? "500px" : "300px"}
          height="auto"
          justifyContent="space-between"
        >
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="nome completo"
            label="Nome completo"
            name="nome completo"
            autoFocus
            onChange={handleUsername}
            value={username}
          />
          <TextField
            variant="outlined"
            margin="normal"
            InputProps={{
              readOnly: true,
            }}
            required
            fullWidth
            id="e-mail"
            label="E-mail"
            name="e-mail"
            value={email}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            InputProps={{
              readOnly: true,
            }}
            id="cpf"
            label="CPF"
            name="cpf"
            value={document}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="celular"
            label="Celular"
            name="celular"
            onChange={handlePhone}
            value={phone}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="data-nascimento"
            label="Data de nascimento"
            name="data-nascimento"
            onChange={handleBirth}
            value={birth}
          />
          <FormLabel component="legend">
            É responsável pelo apartamento?
          </FormLabel>
          <FormControlLabel
            label="Sim"
            control={
              <Radio
                checked={isResponsible === true}
                onChange={handleIsResponsible}
                name="radio-button"
              />
            }
          />
          <FormControlLabel
            label="Não"
            control={
              <Radio
                checked={isResponsible === false}
                onChange={handleIsResponsible}
                name="radio-button"
              />
            }
          />

          <FormControl variant="outlined" fullWidth margin="normal">
            <InputLabel id="outlined-label">Apartamento</InputLabel>
            <Select
              required
              id="block-select"
              value={apartment}
              onChange={handleApartmentSelect}
              label="Bloco"
            >
              {apartments.map((apartment) => {
                return (
                  <MenuItem key={apartment.id} value={apartment}>
                    {apartment.selectItem}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleEditResident}
          >
            Salvar mudanças
          </Button>
          {errorMensage && (
            <Typography component="h6" color="secondary">
              {errorMensage}
            </Typography>
          )}
        </Box>
      </Paper>
    </PageContainer>
  );
});

export { EditResident };
