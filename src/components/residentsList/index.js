import React, { useEffect, useState } from "react";
import { useParams, withRouter } from "react-router-dom";
import {
  deleteResident,
  getResidentsByApartmentsId,
} from "../../services/residents";
import { List } from "../list";
import { ModalAction } from "../modalAction";
import { PageContainer } from "../pageContainer";

const ResidentsList = withRouter(({ history }) => {
  const [residentsData, setResidentsData] = useState();
  const [openModal, setOpenModal] = useState(false);
  const [residentId, setResidentId] = useState("");

  useEffect(() => {
    async function fetchData() {
      await handleResidents();
    }
    fetchData();
  }, []);

  const { id } = useParams();

  const handleResidents = async () => {
    const response = await getResidentsByApartmentsId(id);
    if (typeof response.data === "string") return;
    setResidentsData(response.data);
  };

  const goToCreateResident = () => history.push(`/home/create_resident/${id}`);

  const goToEditResident = (residentId) =>
    history.push(`/home/edit_resident/${residentId}`);

  const goToShowResident = (residentId) =>
    history.push(`/home/show_resident/${residentId}`);

  const createItem = (resindents) => {
    return resindents
      .sort((a, b) => b.is_responsible - a.is_responsible)
      .map((resident) => {
        return {
          id: resident.id,
          data: `Nome: ${resident.username}, Responável: ${
            resident.is_responsible ? "Sim" : "Não"
          }`,
        };
      });
  };

  const closeModal = () => setOpenModal(false);

  const openConfirmResidentDelete = (residentId) => {
    setResidentId(residentId);
    setOpenModal(true);
  };

  const handleDeleteResident = async () => {
    await deleteResident(residentId);
    await handleResidents();
    setOpenModal(false);
  };

  return (
    <PageContainer title="Lista de Moradores">
      <List
        title={
          residentsData
            ? `Apto. ${residentsData?.number}  Bloco ${residentsData?.block}`
            : "Carregando"
        }
        createText="Morador"
        onCreate={goToCreateResident}
        items={
          (residentsData?.residents && createItem(residentsData?.residents)) ||
          []
        }
        onDelete={(residentId) => openConfirmResidentDelete(residentId)}
        onEdit={(residentId) => goToEditResident(residentId)}
        onGoTo={(residentId) => goToShowResident(residentId)}
      />
      <ModalAction
        isOpen={openModal}
        title={"Atenção!"}
        subtitle="Você tem certeza que quer excluir esse morador?"
        onConfirm={handleDeleteResident}
        onClose={closeModal}
      />
    </PageContainer>
  );
});
export { ResidentsList };
