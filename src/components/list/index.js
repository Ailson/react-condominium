import Box from "@material-ui/core/Box";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import React from "react";
import { FixedSizeList } from "react-window";

const useStyles = makeStyles((theme) => ({
  box: {
    backgroundColor: theme.palette.primary.main,
    borderTopLeftRadius: "4px",
    borderTopRightRadius: "4px",
  },
  title: {
    color: "#FFFFFF",
  },
  paper: { margin: theme.spacing(5), height: "300px" },
}));

const List = ({
  title,
  items,
  createText,
  onCreate,
  onDelete,
  onEdit,
  onGoTo,
}) => {
  const classes = useStyles();

  const renderRow = ({ index }) => {
    const item = items[index].data;
    const id = items[index].id;
    return (
      <ListItem button key={id}>
        <ListItemText primary={`${item}`} onClick={() => onGoTo(id)} />
        <DeleteIcon onClick={() => onDelete(id)} />
        <EditIcon onClick={() => onEdit(id)} />
      </ListItem>
    );
  };

  return (
    <Paper elevation={3} className={classes.paper}>
      <Box display="flex" justifyContent="center" className={classes.box}>
        <Typography variant="h6" className={classes.title}>
          {title}
        </Typography>
      </Box>
      <ListItem button onClick={onCreate}>
        <ListItemText primary={`Cadastrar ${createText}`} />
        <AddIcon />
      </ListItem>
      <FixedSizeList
        height={210}
        width={300}
        itemSize={35}
        itemCount={items.length}
      >
        {renderRow}
      </FixedSizeList>
    </Paper>
  );
};

export { List };
