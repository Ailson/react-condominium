import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import Paper from "@material-ui/core/Paper";
import Radio from "@material-ui/core/Radio";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import React, { useState } from "react";
import { useParams, withRouter } from "react-router-dom";
import { createResident } from "../../services/residents";
import { maskBirth, maskCPF, maskPhoneNumber } from "../../utils/helpers";
import { PageContainer } from "../pageContainer";

const CreateResident = withRouter(({ history }) => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [document, setDocument] = useState("");
  const [phone, setPhone] = useState("");
  const [birth, setBirth] = useState("");
  const [isResponsible, setIsResponsible] = useState(true);
  const [errorMensage, setErrorMessage] = useState("");
  const matches = useMediaQuery("(min-width:600px)");
  const { id } = useParams();

  const handleUsername = (e) => setUsername(e.target.value);

  const handleEmail = (e) => setEmail(e.target.value);

  const handleDocument = (e) => setDocument(maskCPF(e.target.value));

  const handlePhone = (e) => setPhone(maskPhoneNumber(e.target.value));

  const handleBirth = (e) => setBirth(maskBirth(e.target.value));

  const handleIsResponsible = () => setIsResponsible(!isResponsible);

  const handleCreateResident = async () => {
    if (!username || !email || !birth || !phone || !document) {
      setErrorMessage("Preencha todos os campos corretamente");
      return;
    }

    const response = await createResident({
      username,
      email,
      birth: birth.split("/").reverse().join("-"),
      phone,
      document,
      is_responsible: isResponsible,
      apartment_id: id,
    });

    if (typeof response.data === "string") {
      setErrorMessage(response.data);
    } else {
      history.goBack();
    }
  };

  return (
    <PageContainer title="Cadastrar Moradores">
      <Paper elevation={3}>
        <Box
          padding="24px"
          width={matches ? "500px" : "300px"}
          height="auto"
          justifyContent="space-between"
        >
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="nome completo"
            label="Nome completo"
            name="nome completo"
            autoFocus
            onChange={handleUsername}
            value={username}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="e-mail"
            label="E-mail"
            name="e-mail"
            onChange={handleEmail}
            value={email}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="cpf"
            label="CPF"
            name="cpf"
            onChange={handleDocument}
            value={document}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="celular"
            label="Celular"
            name="celular"
            onChange={handlePhone}
            value={phone}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="data-nascimento"
            label="Data de nascimento"
            name="data-nascimento"
            onChange={handleBirth}
            value={birth}
          />
          <FormLabel component="legend">
            É responsável pelo apartamento?
          </FormLabel>
          <FormControlLabel
            label="Sim"
            control={
              <Radio
                checked={isResponsible === true}
                onChange={handleIsResponsible}
                name="radio-button"
              />
            }
          />
          <FormControlLabel
            label="Não"
            control={
              <Radio
                checked={isResponsible === false}
                onChange={handleIsResponsible}
                name="radio-button"
              />
            }
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleCreateResident}
          >
            Cadastrar
          </Button>
          {errorMensage && (
            <Typography component="h6" color="secondary">
              {errorMensage}
            </Typography>
          )}
        </Box>
      </Paper>
    </PageContainer>
  );
});

export { CreateResident };
