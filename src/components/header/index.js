import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Input from "@material-ui/icons/Input";
import React from "react";
import { logout } from "../../services/auth";

const Header = ({ history }) => {
  const handleLogout = () => {
    logout();
    history.push("/");
  };

  return (
    <AppBar position="relative">
      <Toolbar>
        <Box
          display="flex"
          flexDirection="row"
          flex="1"
          justifyContent="space-between"
        >
          <Typography variant="h6" color="inherit" noWrap>
            Bem-vindo
          </Typography>
          <Button color="inherit" onClick={handleLogout}>
            <Input />
          </Button>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export { Header };
