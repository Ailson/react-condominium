import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { createApartment } from "../../services/apartments";
import { PageContainer } from "../pageContainer";

const CreateApartment = withRouter(({ history }) => {
  const [number, setNumber] = useState("");
  const [block, setBlock] = useState("");
  const [errorMensage, setErrorMessage] = useState("");
  const matches = useMediaQuery("(min-width:600px)");

  const handleNumber = (e) => setNumber(e.target.value);

  const handleBlock = (e) => setBlock(e.target.value);

  const handleCreateApartment = async () => {
    if (!number || !block) {
      setErrorMessage("Preencha todos os campos");
      return;
    }

    const response = await createApartment(number, block);

    if (typeof response.data === "string") {
      setErrorMessage(response.data);
    } else {
      history.goBack();
    }
  };

  return (
    <PageContainer title="Criar Apartamento">
      <Paper elevation={3}>
        <Box
          padding="24px"
          width={matches ? "500px" : "300px"}
          height="220px"
          justifyContent="space-between"
        >
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="numero"
            label="Número"
            name="numero"
            autoFocus
            onChange={handleNumber}
          />
          <FormControl variant="outlined" fullWidth margin="normal">
            <InputLabel id="outlined-label">Bloco</InputLabel>
            <Select
              required
              id="block-select"
              value={block}
              onChange={handleBlock}
              label="Bloco"
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={"A"}>A</MenuItem>
              <MenuItem value={"B"}>B</MenuItem>
            </Select>
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleCreateApartment}
          >
            Criar
          </Button>
          {errorMensage && (
            <Typography component="h6" color="secondary">
              {errorMensage}
            </Typography>
          )}
        </Box>
      </Paper>
    </PageContainer>
  );
});

export { CreateApartment };
