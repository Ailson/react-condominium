import Box from "@material-ui/core/Box";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import Paper from "@material-ui/core/Paper";
import Radio from "@material-ui/core/Radio";
import TextField from "@material-ui/core/TextField";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import React, { useEffect, useState } from "react";
import { useParams, withRouter } from "react-router-dom";
import { getResidentById } from "../../services/residents";
import { PageContainer } from "../pageContainer";

const ShowResident = withRouter(({ history }) => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [document, setDocument] = useState("");
  const [phone, setPhone] = useState("");
  const [birth, setBirth] = useState("");
  const [isResponsible, setIsResponsible] = useState(true);
  const [apartment, setApartment] = useState();
  const matches = useMediaQuery("(min-width:600px)");
  const { id } = useParams();

  useEffect(() => {
    async function fetchData() {
      await getResident();
    }
    fetchData();
  }, []);

  const getResident = async () => {
    const response = await getResidentById(id);

    if (typeof response === "string") return;

    const apartment =
      response.data.apartments.length > 0 &&
      response.data.apartments
        .filter(
          (apartment) => apartment.id === response.data.resident.apartment_id
        )
        .map((apartment) => {
          return {
            id: apartment.id,
            data: `Bloco: ${apartment.block}, N° ${apartment.number}`,
          };
        });

    setUsername(response.data.resident.username);
    setEmail(response.data.resident.email);
    setDocument(response.data.resident.document);
    setPhone(response.data.resident.phone);
    setBirth(
      response.data.resident.birth.split("T")[0].split("-").reverse().join("/")
    );
    setIsResponsible(response.data.resident.is_responsible);
    setApartment(apartment[0]);
  };

  return (
    <PageContainer title="Exibir Morador">
      <Paper elevation={3}>
        <Box
          padding="24px"
          width={matches ? "500px" : "300px"}
          height="auto"
          justifyContent="space-between"
        >
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="nome completo"
            label="Nome completo"
            name="nome completo"
            autoFocus
            InputProps={{
              readOnly: true,
            }}
            value={username}
          />
          <TextField
            variant="outlined"
            margin="normal"
            InputProps={{
              readOnly: true,
            }}
            required
            fullWidth
            id="e-mail"
            label="E-mail"
            name="e-mail"
            value={email}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            InputProps={{
              readOnly: true,
            }}
            id="cpf"
            label="CPF"
            name="cpf"
            value={document}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="celular"
            label="Celular"
            name="celular"
            InputProps={{
              readOnly: true,
            }}
            value={phone}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="data-nascimento"
            label="Data de nascimento"
            name="data-nascimento"
            InputProps={{
              readOnly: true,
            }}
            value={birth}
          />
          <FormLabel component="legend">
            É responsável pelo apartamento?
          </FormLabel>
          <FormControlLabel
            label="Sim"
            disabled={!isResponsible}
            control={
              <Radio checked={isResponsible === true} name="radio-button" />
            }
          />
          <FormControlLabel
            label="Não"
            disabled={isResponsible}
            control={
              <Radio checked={isResponsible === false} name="radio-button" />
            }
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="apartamento"
            label="Apartamento"
            name="apartamento"
            InputProps={{
              readOnly: true,
            }}
            value={apartment?.data}
          />
        </Box>
      </Paper>
    </PageContainer>
  );
});

export { ShowResident };
