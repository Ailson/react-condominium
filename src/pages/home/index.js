import React from "react";
import { Route, withRouter } from "react-router-dom";
import {
  ApartmentsList,
  CreateApartment,
  CreateResident,
  EditApartment,
  EditResident,
  Header,
  ResidentsList,
  ShowResident,
} from "../../components/index";

const Home = withRouter(({ history }) => (
  <>
    <Header history={history} />
    <Route exact path="/home" component={ApartmentsList} />
    <Route exact path="/home/create_apartment" component={CreateApartment} />
    <Route exact path="/home/edit_apartment/:id" component={EditApartment} />
    <Route exact path="/home/residents/:id" component={ResidentsList} />
    <Route exact path="/home/create_resident/:id" component={CreateResident} />
    <Route exact path="/home/edit_resident/:id" component={EditResident} />
    <Route exact path="/home/show_resident/:id" component={ShowResident} />
  </>
));

export default Home;
