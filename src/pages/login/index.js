import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import LocationCity from "@material-ui/icons/LocationCity";
import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { login } from "../../services/auth";
import { loginUser } from "../../services/login";

const Login = withRouter(({ history }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMensage, setErrorMessage] = useState("");
  const matches = useMediaQuery("(min-width:600px)");

  const handleEmail = (e) => setEmail(e.target.value);

  const handlePassword = (e) => setPassword(e.target.value);

  const handleLogin = async () => {
    const response = await loginUser(email, password);
    if (response.status !== 200) {
      setErrorMessage(
        "Não foi possível fazer o login. Verifique suas credencials"
      );
    } else {
      login(response.data.token);
      history.push("/home");
    }
  };

  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: "100vh", padding: "0px 24px" }}
    >
      <Box
        display="flex"
        flexDirection="column"
        width={matches ? "500px" : "300px"}
        alignItems="center"
      >
        <LocationCity color="primary" fontSize="large" />
        <Typography component="h1" variant="h5">
          Bem-vindo ao Condominium
        </Typography>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          autoFocus
          onChange={handleEmail}
        />

        <TextField
          type="password"
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="paswword"
          label="Senha"
          name="paswword"
          autoComplete="current-password"
          onChange={handlePassword}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          onClick={handleLogin}
        >
          Sign In
        </Button>
        {errorMensage && (
          <Typography component="h6" color="secondary">
            {errorMensage}
          </Typography>
        )}
      </Box>
    </Grid>
  );
});

export default Login;
