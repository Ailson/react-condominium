import axios from "axios";
import { getToken } from "./auth";

const baseURL =
  process.env.NODE_ENV === "development"
    ? "http://127.0.0.1:3333"
    : "https://polar-sierra-22334.herokuapp.com/";

const api = axios.create({
  baseURL,
});

api.interceptors.request.use(async (config) => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default api;
