import api from "./api";

export const getApartments = async () => {
  try {
    return await api.get("/apartments");
  } catch (error) {
    return error;
  }
};

export const getApartmentById = async (id) => {
  try {
    return await api.get(`/apartments/${id}`);
  } catch (error) {
    return error;
  }
};

export const updateApartment = async ({ id, number, block }) => {
  try {
    return await api.put(`/apartments/${id}`, { number, block });
  } catch (error) {
    return error;
  }
};

export const createApartment = async (number, block) => {
  try {
    return await api.post("/apartments", { number, block });
  } catch (error) {
    return error;
  }
};

export const deleteApartment = async (id) => {
  try {
    return await api.delete(`/apartments/${id}`);
  } catch (error) {
    return error;
  }
};
