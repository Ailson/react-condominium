import api from "./api";

export const getResidentsByApartmentsId = async (id) => {
  try {
    return await api.get(`residents/apartment_id/${id}`);
  } catch (error) {
    return error;
  }
};

export const createResident = async ({
  username,
  email,
  document,
  phone,
  birth,
  is_responsible,
  apartment_id,
}) => {
  try {
    return api.post("/residents", {
      username,
      email,
      document,
      phone,
      birth,
      is_responsible,
      apartment_id,
    });
  } catch (error) {
    return error;
  }
};

export const deleteResident = async (id) => {
  try {
    return await api.delete(`/residents/${id}`);
  } catch (error) {
    return error;
  }
};

export const getResidentById = async (id) => {
  try {
    return await api.get(`/residents/${id}`);
  } catch (error) {
    return error;
  }
};

export const updateResident = async ({
  id,
  username,
  phone,
  birth,
  is_responsible,
  apartment_id,
}) => {
  try {
    return await api.put(`/residents/${id}`, {
      username,
      phone,
      birth,
      is_responsible,
      apartment_id,
    });
  } catch (error) {
    return error;
  }
};
