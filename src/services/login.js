import api from "./api";

export const loginUser = async (email, password) => {
  try {
    return await api.post("/authenticate", { email, password });
  } catch (error) {
    return error;
  }
};
