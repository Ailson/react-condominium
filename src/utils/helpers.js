export const maskBirth = (birth) => {
  const date = birth.replace(/\D/g, "");
  const char = { 2: "/", 4: "/" };
  let r = "";
  for (let i = 0; i < date.length; i++) {
    r += (char[i] || "") + date[i];
  }
  return r.substr(0, 10);
};

export const maskPhoneNumber = (phone) => {
  const numbers = phone.replace(/\D/g, "");
  const char = { 0: "(", 2: ") ", 3: " ", 7: "-" };
  let r = "";
  for (let i = 0; i < numbers.length; i++) {
    r += (char[i] || "") + numbers[i];
  }
  return r.substr(0, 16);
};

export const maskCPF = (v) => {
  if (v.length >= 13) return v.substr(0, 14);

  v = v.replace(/\D/g, "");
  v = v.replace(/(\d{3})(\d)/, "$1.$2");
  v = v.replace(/(\d{3})(\d)/, "$1.$2");

  v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
  return v;
};

export const removeMask = (value) => value.replace(/\W/g, "");
